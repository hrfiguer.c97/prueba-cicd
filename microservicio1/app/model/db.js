'user strict';

var mysqldb = require('mysql');

var conexion = mysqldb.createConnection({
    host:       process.env.MYSQL_HOST ? process.env.MYSQL_HOST : '172.17.0.2',
    user:       process.env.MYSQL_USER ? process.env.MYSQL_USER : 'root',
    password:   process.env.MYSQL_PASSWORD ? process.env.MYSQL_PASSWORD : '1234',
    port:       process.env.MYSQL_PORT ? process.env.MYSQL_PORT : 33060,
    database:   process.env.MYSQL_DATABASE ? process.env.MYSQL_DATABASE : 'practica1'
});

conexion.connect(function(err) {
    if(err){
        throw err;
    }
    else {
        console.log('Conexion exitosa!');
    }
})

module.exports = conexion;