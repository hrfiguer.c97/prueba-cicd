'use strict';

module.exports = function(app) {
    
    var ordenes = require('../controller/appController');

    app.route('/')
        .get(ordenes.index);
}