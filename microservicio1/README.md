**Actualizar estado de orden**
----
  Ruta para actualizar el estado de una orden creada anteriormente.

* **URL**

  /actualizar_estado_orden

* **Method:**

  `PUT`
  
*  **URL Params**

   **Required:**
 
   `estado=[string]`

   `id_orden=[integer]`


**Actualizar total de orden**
----
  Ruta para actualizar el total de una orden creada anteriormente.

* **URL**

  /actualizar_total_orden

* **Method:**

  `PUT`
  
*  **URL Params**

   **Required:**
 
   `total=[integer]`

   `id_orden=[integer]`


**Borrar orden**
----
  Ruta para eliminar una orden creada por su id.

* **URL**

  /borrar_orden

* **Method:**

  `DELETE`
  
*  **URL Params**

   **Required:**

   `id_orden=[integer]`


**Crear orden**
----
  Ruta para crear una orden nueva.

* **URL**

  /crear_orden

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**
 
   `total=[integer]`

   `fecha=[string]`

   `direccion=[string]`

   `id_usuario=[integer]`

   `estado=[string]`


**Obtener ordenes**
----
  Ruta para obtener todas las ordenes que se han creado.

* **URL**

  /leer_orden

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**


**Obtener orden por su id**
----
  Ruta para obtener el detalle de una sola orden por su id.

* **URL**

  /leer_orden

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**

   `id_orden=[integer]`